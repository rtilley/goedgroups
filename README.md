# goedgroups

A go program that demonstrates how to use the ED Group Management web service. You may use this code to programatically add, delete and list the members of your ED groups.

## To build
``` make ```

## Some Usage Examples

  * goedgroups -list -key /home/rbt/Downloads/itso-netscan.key -group iso.bugbounty
  * goedgroups -add -key /home/rbt/Downloads/itso-netscan.key -group iso.bugbounty -pid rtilley
  * goedgroups -del -key /home/rbt/Downloads/itso-netscan.key -group iso.bugbounty -pid rtilley
  * goedgroups -in -key /home/rbt/Downloads/itso-netscan.key -group iso.bugbounty -pid rtilley

```Usage of ./goedgroups:
  -add
    	Add a member to the specified group. Requires -group -pid and -key
  -del
    	Delete a member from the specified group. Requires -group -pid and -key
  -group string
    	The name of the ED group.
  -help
    	Show help.
  -in
    	See if a member is in the group. Requires -group -pid and -key
  -key string
    	The path to the ED-ID service key.
  -list
    	List members of the specified group. Requires -group and -key
  -pid string
    	The PID of the member to add or delete from the group.
```

## External JWT Library Documentation

  * https://github.com/dgrijalva/jwt-go

## Middleware ED Group Documentation

  * https://www.middleware.vt.edu/ed/groups/

