package main

import (
	"bytes"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

var baseURL = "https://apps.middleware.vt.edu/ws/v1"

// struct to represent an ED group with only person types
type Group struct {
	Uugid          string      `json:"uugid"`
	DisplayName    interface{} `json:"displayName"` // can be null
	CreationDate   string      `json:"creationDate"`
	ExpirationDate string      `json:"expirationDate"`
	Members        []struct {
		UID            int         `json:"uid"`
		Pid            string      `json:"pid"`
		DisplayName    string      `json:"displayName"`
		CreationDate   string      `json:"creationDate"`
		ExpirationDate interface{} `json:"expirationDate"` // can be null
		Kind           string      `json:"kind"`
	} `json:"members"`
}

// addMember - add a PID to an ED group. Return the HTTP status code.
// returns 201 (created) when PID is successfully added to the group.
// returns 409 (conflict) when PID (that is already in the group) is added again.
// returns 404 (not found) when the given PID is not a valid PID.
func addMember(token, group, pid string, client *http.Client) int {
	data := url.Values{}
	data.Add("kind", "person")
	data.Add("id", pid)

	req, err := http.NewRequest("POST", baseURL+"/groups/"+group+"/members", bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Authorization", "Bearer "+token)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	return resp.StatusCode
}

// delMember - delete a PID from an ED group. Return the HTTP status code.
// returns 204 (no content) when valid PID is deleted from a group.
// returns 404 (not found) when valid PID is not a member of the group.
func delMember(token, group, pid string, client *http.Client) int {
	req, err := http.NewRequest("DELETE", baseURL+"/groups/"+group+"/members/"+pid, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	return resp.StatusCode
}

// listMembers - list the members of an ED group. Return the raw json as []byte.
func listMembers(token, group string, client *http.Client) []byte {
	req, err := http.NewRequest("GET", baseURL+"/groups/"+group+"?with=members", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(body))
	return body
}

func main() {
	// Flags
	var add = flag.Bool("add", false, "Add a member to the specified group. Requires -group -pid and -key")
	var del = flag.Bool("del", false, "Delete a member from the specified group. Requires -group -pid and -key")
	var list = flag.Bool("list", false, "List members of the specified group. Requires -group and -key")
	var in = flag.Bool("in", false, "See if a member is in the group. Requires -group -pid and -key")
	var key = flag.String("key", "", "The path to the ED-ID service key.")
	var pid = flag.String("pid", "", "The PID of the member to add or delete from the group.")
	var group = flag.String("group", "", "The name of the ED group.")
	var help = flag.Bool("help", false, "Show help.")

	flag.Parse()
	if *help {
		flag.PrintDefaults()
		return
	}

	if *key == "" {
		log.Fatalf("You must provide a key file -key /path/to/your/key\n")
	}

	if *group == "" {
		log.Fatalf("You must provide a group\n")
	}

	// Setup ED-ID Service key
	keyBytes, err := ioutil.ReadFile(*key)
	if err != nil {
		log.Fatal(err)
	}

	//block, rest := pem.Decode(keyBytes)
	block, _ := pem.Decode(keyBytes)
	if block == nil || block.Type != "PRIVATE KEY" {
		log.Fatal("failed to decode PEM block containing private key")
	}

	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		log.Fatalf("key parsing error: %v\n", err)
	}

	//fmt.Printf("Got a %T, with remaining data: %q\n", privateKey, rest)

	// Setup JWT to expire in 1 minute
	now := time.Now()
	oneMinute := now.Add(time.Minute * 1)

	claims := jwt.MapClaims{
		"iss": "uusid=itso-netscan,ou=services,dc=vt,dc=edu",
		"iat": now.Unix(),
		"exp": oneMinute.Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	tokenString, err := token.SignedString(privateKey)

	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	if *add {
		code := addMember(tokenString, *group, *pid, client)
		if code != 201 {
			log.Fatalf("addMember returned: %d %s\n", code, http.StatusText(code))
		}
	}

	if *del {
		code := delMember(tokenString, *group, *pid, client)
		if code != 204 {
			log.Fatalf("delMember returned: %d %s\n", code, http.StatusText(code))
		}
	}

	if *list {
		body := listMembers(tokenString, *group, client)

		var edGroup Group
		err = json.Unmarshal(body, &edGroup)
		if err != nil {
			log.Println("error:", err)
		}

		fmt.Printf("There are %d members in %s:\n", len(edGroup.Members), *group)

		for i, member := range edGroup.Members {
			fmt.Printf("%d. %v - %v %v\n", i, member.Pid, member.DisplayName, member.CreationDate)
		}
	}

	if *in {
		body := listMembers(tokenString, *group, client)

		var edGroup Group
		err = json.Unmarshal(body, &edGroup)
		if err != nil {
			log.Println("error:", err)
		}

		init := false
		cdate := ""

		for _, member := range edGroup.Members {
			if member.Pid == *pid {
				init = true
				cdate = member.CreationDate
				break
			}
		}

		if init {
			fmt.Printf("%s contains %s as of %s\n", *group, *pid, cdate)
		} else {
			fmt.Printf("%s does not contain %s\n", *group, *pid)
		}
	}
}
